define mariadb::createuser ( 
	$username = '',
	$password = '',
	$hostname = 'localhost',
	$filename = ''
	)

{
file {"$filename" :
	content => template('mariadb/user.sh.erb'),
	path => "/home/vagrant/$filename",
}

exec {"$username":
	command => "/bin/bash /home/vagrant/$filename",
	require => File ["$filename"]
}

}
