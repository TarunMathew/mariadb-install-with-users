class mariadb::install {
        package {'mariadb-server':
                ensure => latest,
         }
        service {'mariadb':
                ensure => running,
                require => Package['mariadb-server'],
        }
}

